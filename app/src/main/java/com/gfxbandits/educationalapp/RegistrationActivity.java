package com.gfxbandits.educationalapp;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class RegistrationActivity extends AppCompatActivity {
  
  //registration fields
  private EditText name, email, phone, password, confirmPassword;
  private Button register;
  private String registration_url = "http://letsweb.in/demo/sakecworkshop/register.php";
  //progress dialog to show that registration process is on
  private ProgressDialog progress;
  
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_registration);
    
    //initialise views
    name = (EditText) findViewById(R.id.register_name);
    email = (EditText) findViewById(R.id.register_email);
    phone = (EditText) findViewById(R.id.register_phone);
    password = (EditText) findViewById(R.id.register_password);
    confirmPassword = (EditText) findViewById(R.id.register_confim_password);
    
    register = (Button) findViewById(R.id.register_button);
    register.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {
        checkParams();
      }
    });
    
    //initialise progressDialog
    progress = new ProgressDialog(this);
    progress.setMessage("Registering...");
  }
  
  //method to check fields
  private void checkParams() {
    if (name.getText().toString().length() < 3) {
      Toast.makeText(this, "Name is too short", Toast.LENGTH_SHORT).show();
    }
    //use android's email pattern checker
    else if (!Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
      Toast.makeText(this, "Invalid email address", Toast.LENGTH_SHORT).show();
    }
    //check length of phone
    else if (phone.getText().toString().length() < 10) {
      Toast.makeText(this, "Phone number should be of 10 digits", Toast.LENGTH_SHORT).show();
    }
    //match password and confirm password fields
    else if (password.getText().toString().equals(confirmPassword.getText().toString())) {
      Toast.makeText(this, "Passwords do not match", Toast.LENGTH_SHORT).show();
    }
    //everything is ok, we can now do registration process
    else {
      doRegistration();
    }
  }
  
  //do registration through api call
  private void doRegistration() {
    //show progressDialog
    progress.show();
    StringRequest req = new StringRequest(
        //method to call api
        Method.POST,
        //link to hit
        registration_url,
        //a response listener to listen and read the response from the api
        new Listener<String>() {
          @Override
          public void onResponse(String response) {
            Log.e("registration resp", response);
            //parse the json response and act accordingly
            try{
              JSONObject res = new JSONObject(response);
              if(res.getString("status").equalsIgnoreCase("success")){
                //registered successfully, close registration activity
                Toast.makeText(RegistrationActivity.this, "Successfully registered", Toast.LENGTH_SHORT).show();
                RegistrationActivity.this.finish();
              }else{
                //registration failed, show failure message
                Toast.makeText(RegistrationActivity.this, res.getString("message"), Toast.LENGTH_SHORT).show();
              }
            }catch (JSONException e){
              e.printStackTrace();
            }
            progress.dismiss();
          }
        },
        //error listener when a network error occurs
        new ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {
            error.printStackTrace();
            Toast.makeText(RegistrationActivity.this, "Internet connection error", Toast.LENGTH_SHORT).show();
            progress.dismiss();
          }
        }) {
      @Override
      protected Map<String, String> getParams() throws AuthFailureError {
        //send registration paramters to api through key,value pair (map)
        Map<String, String> params = new HashMap<>();
        params.put("name", name.getText().toString());
        params.put("emailid", email.getText().toString());
        params.put("phone", phone.getText().toString());
        params.put("password", password.getText().toString());
        return params;
      }
    };
    //add the StringRequest to network request queue
    Volley.newRequestQueue(this).add(req);
  }
}

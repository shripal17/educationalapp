package com.gfxbandits.educationalapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class OurCoursesActivity extends AppCompatActivity implements OnClickListener {
  
  private ImageView course_1, course_2, course_3, course_4,course_5;
  
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_our_courses);
    
    //initialise images
    course_1 = (ImageView) findViewById(R.id.course_1);
    course_2 = (ImageView) findViewById(R.id.course_2);
    course_3 = (ImageView) findViewById(R.id.course_3);
    course_4 = (ImageView) findViewById(R.id.course_4);
    course_5 = (ImageView) findViewById(R.id.course_5);
    
    //set onclick listeners
    course_1.setOnClickListener(this);
    course_2.setOnClickListener(this);
    course_3.setOnClickListener(this);
    course_4.setOnClickListener(this);
    course_5.setOnClickListener(this);
  }
  @Override
  public void onClick(View view) {
    switch (view.getId()) {
      case R.id.course_1:
        openVideo("1.mp4");
        break;
      
      case R.id.course_2:
        openVideo("2.mp4");
        break;
      
      case R.id.course_3:
        openVideo("3.mp4");
        break;
      
      case R.id.course_4:
        openVideo("4.mp4");
        break;
      
      case R.id.course_5:
        openVideo("5.mp4");
        break;
    }
  }
  
  //method to open videoActivity and send link to it
  private void openVideo(String url){
    Intent openVid = new Intent(OurCoursesActivity.this, VideoActivity.class);
    openVid.putExtra("videoURL", "http://letsweb.in/demo/sakecworkshop/video/" + url);
    startActivity(openVid);
  }
}

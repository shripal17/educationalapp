package com.gfxbandits.educationalapp;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class HomeActivity extends AppCompatActivity {
  
  private ImageView aboutUs, ourCourses, rateUs, contactUs;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_home);
    
    aboutUs = (ImageView) findViewById(R.id.about_us);
    ourCourses = (ImageView) findViewById(R.id.our_courses);
    rateUs = (ImageView) findViewById(R.id.rate_us);
    contactUs = (ImageView) findViewById(R.id.contact_us);
    
    //set onclick listeners
    aboutUs.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {
        //open AboutActivity
        startActivity(new Intent(HomeActivity.this, AboutActivity.class));
      }
    });
    
    ourCourses.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {
        //open OurCoursesActivity
        startActivity(new Intent(HomeActivity.this, OurCoursesActivity.class));
      }
    });
    
    rateUs.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {
        //open Google app on play store
        Intent openPlayStore = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.googlequicksearchbox"));
        startActivity(openPlayStore);
      }
    });
    
    contactUs.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {
        //open CotnactUsActivity
        startActivity(new Intent(HomeActivity.this, ContactUsActivity.class));
      }
    });
  }
}

package com.gfxbandits.educationalapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {
  
  //view elements
  private EditText email_field, password_field;
  private Button login_button, register_button;
  private String login_url = "http://letsweb.in/demo/sakecworkshop/login.php";
  //progress dialog to show that login process is on
  private ProgressDialog progress;
  
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);
    
    //initialise view elements to use them throughout our activity
    login_button = (Button) findViewById(R.id.login_button);
    register_button = (Button) findViewById(R.id.login_register_button);
    email_field = (EditText) findViewById(R.id.login_email);
    password_field = (EditText) findViewById(R.id.login_password);
    
    //initialise progressDialog
    progress = new ProgressDialog(this);
    progress.setMessage("Logging in...");
    
    //what to do when login button is clicked
    login_button.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {
        checkParams();
        //startActivity(new Intent(LoginActivity.this, HomeActivity.class));
      }
    });
    
    //what to do when register button is clicked
    register_button.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {
        //open RegistrationActivity
        Intent openRegister = new Intent(LoginActivity.this, RegistrationActivity.class);
        startActivity(openRegister);
      }
    });
  }
  
  private void checkParams() {
    //check if email is valid or password length is less than 4
    //use android's email pattern checker
    if (!Patterns.EMAIL_ADDRESS.matcher(email_field.getText().toString()).matches()) {
      Toast.makeText(LoginActivity.this, "Invalid email", Toast.LENGTH_SHORT).show();
    } else if (password_field.getText().toString().length() < 4) {
      Toast.makeText(LoginActivity.this, "Password must be of minimum 4 characters", Toast.LENGTH_SHORT).show();
    } else {
      //now we can do login
      doLogin();
    }
  }
  
  private void doLogin() {
    //first show progressDialog, then do login process
    progress.show();
    
    //Login api response is a string, so we create a StringRequest object
    StringRequest req = new StringRequest(
        //method to call api
        Method.POST,
        //link to hit
        login_url,
        //a response listener to listen and read the response from the api
        new Listener<String>() {
          @Override
          public void onResponse(String response) {
            Log.e("login resp", response);
            //parse the response in json
            try {
              JSONObject res = new JSONObject(response);
              //check the 'status' key from json response
              if (res.getString("status").equalsIgnoreCase("success")) {
                //login is successful, let's go ahead
                startActivity(new Intent(LoginActivity.this, HomeActivity.class));
              } else {
                //login failed, show the failure message
                Toast.makeText(LoginActivity.this, res.getString("message"), Toast.LENGTH_SHORT).show();
              }
            } catch (JSONException e) {
              //catch JSON errors/exceptions
              e.printStackTrace();
            }
            //we are done with processing server response, hide the progress
            progress.dismiss();
          }
        },
        //error listener when a network error occurs
        new ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {
            //if error occurs
            error.printStackTrace();
            Toast.makeText(LoginActivity.this, "Internet connection error", Toast.LENGTH_SHORT).show();
            //dismiss (hide) the progress dialog
            progress.dismiss();
          }
        }) {
      @Override
      protected Map<String, String> getParams() throws AuthFailureError {
        //create a key, value pair (map) for sending parameters to the Login API
        Map<String, String> params = new HashMap<String, String>();
        params.put("emailid", email_field.getText().toString());
        params.put("password", password_field.getText().toString());
        return params;
      }
    };
    //Add the StringRequest object to the network request queue
    Volley.newRequestQueue(getApplicationContext()).add(req);
  }
}

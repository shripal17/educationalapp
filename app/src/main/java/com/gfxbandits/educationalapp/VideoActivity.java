package com.gfxbandits.educationalapp;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

public class VideoActivity extends AppCompatActivity {
  
  private VideoView videoView;
  
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_video);
    
    videoView = (VideoView) findViewById(R.id.videoView);
    
    //inform the user on error
    videoView.setOnErrorListener(new OnErrorListener() {
      //refer to https://developer.android.com/reference/android/media/MediaPlayer.OnErrorListener.html for parameters
      @Override
      public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {
        Toast.makeText(VideoActivity.this, "Failed to play the video", Toast.LENGTH_SHORT).show();
        return true;
      }
    });
    
    //close activity when video completes
    videoView.setOnCompletionListener(new OnCompletionListener() {
      @Override
      public void onCompletion(MediaPlayer mediaPlayer) {
        VideoActivity.this.finish();
      }
    });
    
    //set play/pause, seek, forward, rewind controller
    MediaController mediaController = new MediaController(this);
    mediaController.setAnchorView(videoView);
    videoView.setMediaController(mediaController);
    
    //set video link
    String link = getIntent().getStringExtra("videoURL");
    videoView.setVideoURI(Uri.parse(link));
    
    //start the video
    videoView.requestFocus();
    videoView.start();
  }
  
  @Override
  protected void onPause() {
    super.onPause();
    try{
      videoView.pause();
    }catch (Exception e){
      e.printStackTrace();
    }
  }
  
  @Override
  protected void onResume() {
    super.onResume();
    try{
      videoView.resume();
    }catch (Exception e){
      e.printStackTrace();
    }
  }
}

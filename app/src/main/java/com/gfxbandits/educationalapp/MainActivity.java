package com.gfxbandits.educationalapp;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
  
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    //open LoginActivity after 2000milliseconds
    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        Intent openLogin = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(openLogin);
        //finish(close) this activity
        finish();
      }
    }, 2000);
  }
}
